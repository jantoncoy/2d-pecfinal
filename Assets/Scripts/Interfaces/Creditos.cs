using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Creditos : MonoBehaviour
{
    /// <summary>
    /// Indica si esta parada la camara
    /// </summary>
    private bool parar = false;

    // Update is called once per frame
    void Update()
    {
        if (!parar)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 0.05f, this.transform.position.z);
        }
    }

    /// <summary>
    /// Cuando se detecta colision se detiene y vuelve al menu
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        parar = true;
        Invoke("cargarMenu", 2.5f);
    }

    /// <summary>
    /// Continua el juego
    /// </summary>
    public void cargarMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
