using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class EventosInterfaces
{
    /// <summary>
    /// Muestra una accion
    /// </summary>
    public static Action<int> mostrarAccion;

    /// <summary>
    /// Muestra una accion con personalizacion
    /// </summary>
    public static Action<int,string> mostrarAccionPersonalizado;

    /// <summary>
    /// Oculta una accion
    /// </summary>
    public static Action ocultarAccion;

    /// <summary>
    /// Muestra el mapa del sector
    /// </summary>
    public static Action mostrarMapa;

    /// <summary>
    /// Oculta el mapa del sector
    /// </summary>
    public static Action ocultarMapa;

    /// <summary>
    /// Se pierde la partida
    /// </summary>
    public static Action perder;

    /// <summary>
    /// Muestra el panel de obtener dinero
    /// </summary>
    public static Action<int> mostrarPanel;

    /// <summary>
    /// deshabilita al personaje en una estacion
    /// </summary>
    public static Action deshabilitarPersonaje;

    /// <summary>
    /// habilita el personaje en una estacion
    /// </summary>
    public static Action habilitarPersonaje;

    /// <summary>
    /// Indica el dialogo que se debe de mostrar
    /// </summary>
    public static Action<string,string> informarDialogo;

    /// <summary>
    /// Pasa al siguiente dialogo
    /// </summary>
    public static Action pasarDialogo;

    /// <summary>
    /// Oculta el panel de dialogos
    /// </summary>
    public static Action ocultarDialogo;

    /// <summary>
    /// Muestra un dialogo
    /// </summary>
    public static Action mostrarDialogo;
}
