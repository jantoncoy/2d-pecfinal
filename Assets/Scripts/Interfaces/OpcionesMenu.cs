using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpcionesMenu : MonoBehaviour
{
    /// <summary>
    /// Salir del juego
    /// </summary>
    public void salirJuego()
    {
        Application.Quit(1);
    }

    /// <summary>
    /// Continua el juego
    /// </summary>
    public void continuarJuego()
    {
        if (PartidaEventos.existePartida())
        {
            PartidaEventos.cargar();
            SceneManager.LoadScene(PartidaEventos.partida.scenaActual);
        }
        else
        {
            Partida nuevaPartida = new Partida();
            nuevaPartida.dinero = 100;
            nuevaPartida.vidaNave = 100;
            nuevaPartida.xNave = 0;
            nuevaPartida.yNave = -15;
            nuevaPartida.zNave = 0;
            nuevaPartida.arma1 = 20;
            nuevaPartida.arma2 = 100;
            nuevaPartida.scenaActual = "Sector_Alfa";
            PartidaEventos.partida = nuevaPartida;

            SceneManager.LoadScene("Sector_Alfa");
        }
        
    }

    /// <summary>
    /// Carga creditos
    /// </summary>
    public void cargarCreditos()
    {
        SceneManager.LoadScene("Creditos");
    }

    /// <summary>
    /// Carga menu
    /// </summary>
    public void cargarMenu()
    {
        SceneManager.LoadScene("Menu");
    }

}
