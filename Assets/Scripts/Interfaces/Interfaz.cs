using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Interfaz : MonoBehaviour
{
    /// <summary>
    /// Texto de la accion
    /// </summary>
    public TextMeshProUGUI textoAccion;

    /// <summary>
    /// Gameobject de la accion
    /// </summary>
    public GameObject iconoAccion;

    /// <summary>
    /// Gameobject del texto
    /// </summary>
    public GameObject textoAccionGameObject;

    /// <summary>
    /// Determina los mensajes para la acciones
    /// </summary>
    private string[] acciones;

    /// <summary>
    /// Nombre del personaje
    /// </summary>
    public TextMeshProUGUI nombre;

    /// <summary>
    /// Dialogo
    /// </summary>
    public TextMeshProUGUI dialogo;

    /// <summary>
    /// Interfaz que se compone del dialogo y el nombre de quien lo dice
    /// </summary>
    public GameObject interfazDialogo;

    /// <summary>
    /// Dinero que se tiene
    /// </summary>
    public TextMeshProUGUI dinero;

    // Start is called before the first frame update
    void Start()
    {
        acciones = new string[5];
        acciones[0] = "Hablar con ";
        acciones[1] = "Comerciar con ";
        acciones[2] = "Recoger ";
        acciones[3] = "Entrar en la estacion ";
        acciones[4] = "Salir de la estacion ";

        EventosInterfaces.mostrarAccion = this.mostrarAccion;
        EventosInterfaces.mostrarAccionPersonalizado = this.mostrarAccionPersonalizacion;
        EventosInterfaces.ocultarAccion = this.ocultarAccion;
        EventosInterfaces.informarDialogo = this.informarDialogo;
        EventosInterfaces.mostrarDialogo = this.mostrarDialogo;
        EventosInterfaces.ocultarDialogo = this.ocultarDialogo;

    }

    // Update is called once per frame
    void Update()
    {
        this.dinero.text = PartidaEventos.partida.dinero+"";
    }

    /// <summary>
    /// Muestra la accion por la interfaz
    /// </summary>
    /// <param name="mensaje"></param>
    private void mostrarAccion(int mensaje)
    {
        textoAccion.text = acciones[mensaje];
        showAccion();
    }

    /// <summary>
    /// Muestra la accion de forma personalizada por la interfaz
    /// </summary>
    /// <param name="mensaje"></param>
    /// <param name="personalizacion"></param>
    private void mostrarAccionPersonalizacion(int mensaje, string personalizacion)
    {
        textoAccion.text = acciones[mensaje]+personalizacion;
        showAccion();
    }

    /// <summary>
    /// Muestra la accion 
    /// </summary>
    private void showAccion()
    {
        if(textoAccionGameObject != null)
            textoAccionGameObject.SetActive(true);
        if(textoAccion != null)
            iconoAccion.SetActive(true);
    }

    /// <summary>
    /// oculta la accion
    /// </summary>
    private void ocultarAccion()
    {
        if (textoAccionGameObject != null)
            textoAccionGameObject.SetActive(false);
        if (textoAccion != null)
            iconoAccion.SetActive(false);
    }

    /// <summary>
    /// Indica que valores tiene el dialogo
    /// </summary>
    /// <param name="nombre"></param>
    /// <param name="dialogo"></param>
    public void informarDialogo(string nombre, string dialogo)
    {
        this.nombre.text = nombre;
        this.dialogo.text = dialogo;
    }

    /// <summary>
    /// muestra el dialogo
    /// </summary>
    public void mostrarDialogo()
    {
        interfazDialogo.SetActive(true);
    }

    /// <summary>
    /// oculta el dialogo
    /// </summary>
    public void ocultarDialogo()
    {
        interfazDialogo.SetActive(false);
    }
}
