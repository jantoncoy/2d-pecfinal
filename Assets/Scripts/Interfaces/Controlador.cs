using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Controlador : MonoBehaviour
{
    /// <summary>
    /// Camara del mapa
    /// </summary>
    public GameObject mapa;

    /// <summary>
    /// Indica si esta activo o no
    /// </summary>
    private bool menu;

    /// <summary>
    /// Canvas de GameOver
    /// </summary>
    public GameObject gameOver;

    /// <summary>
    /// Canvas de UI
    /// </summary>
    public GameObject ui;

    /// <summary>
    /// panel de dinero
    /// </summary>
    public GameObject panelDinero;

    /// <summary>
    /// panel de dinero
    /// </summary>
    public TextMeshProUGUI dinero;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("inicializar", 0.01f);
    }

    /// <summary>
    /// Inicializa las variables
    /// </summary>
    private void inicializar()
    {
        menu = false;
        EventosInterfaces.mostrarMapa = mostrarMapa;
        EventosInterfaces.perder = mostrarGameOver;
        EventosInterfaces.mostrarPanel = mostrarPanel;
        ocultarMapa();
    }

    /// <summary>
    /// Alterna mostrar o no el mapa
    /// </summary>
    private void mostrarMapa()
    {
        menu = !menu;
        if (menu)
            mapa.SetActive(true);
        else
            ocultarMapa();
    }

    /// <summary>
    /// Oculta el mapa
    /// </summary>
    private void ocultarMapa()
    {
        mapa.SetActive(false);
    }

    /// <summary>
    /// Muestra la venta de gameOver
    /// </summary>
    private void mostrarGameOver()
    {
        this.ui.SetActive(false);
        this.gameOver.SetActive(true);
    }

    /// <summary>
    /// Muestra la recoleccion de dinero
    /// </summary>
    private void mostrarPanel(int cantidad)
    {
        PartidaEventos.partida.dinero += cantidad;
        dinero.text = "+" + cantidad;
        this.panelDinero.SetActive(true);
        CancelInvoke("ocultarPanel");
        Invoke("ocultarPanel",1.250f);
    }

    /// <summary>
    /// oculta la recoleccion de dinero
    /// </summary>
    private void ocultarPanel()
    {
        this.panelDinero.SetActive(false);
    }


}
