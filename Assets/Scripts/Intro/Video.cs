using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Video : MonoBehaviour
{
    /// <summary>
    /// Hace referencia al reproductor de video
    /// </summary>
    UnityEngine.Video.VideoPlayer video;

    // Start is called before the first frame update
    void Start()
    {
        video = GetComponent<UnityEngine.Video.VideoPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (video.frame == (video.frameCount - 1.0f))
        {
            //Pasamos al menu una vez que termina la intro
            SceneManager.LoadScene("Menu");
        }
    }
}
