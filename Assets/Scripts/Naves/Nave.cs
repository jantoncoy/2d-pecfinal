using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Nave : MonoBehaviour
{

    /// <summary>
    /// Velocidad de la nave (0 = parado, 1 = velocidad normal, 2 = velocidad maxima)
    /// </summary>
    public int velocidad = 0;

    /// <summary>
    /// Indica si es un npc esta nave
    /// </summary>
    public bool isNPC = true;

    /// <summary>
    /// Comandos de la nave
    /// </summary>
    public bool up, down, left, right, action, secondary, menu;

    /// <summary>
    /// Camara vinculada
    /// </summary>
    public GameObject camaraVinculada = null;

    /// <summary>
    /// Rigidbody de la nave
    /// </summary>
    private Rigidbody2D rigid = null;

    /// <summary>
    /// Animacion de la nave
    /// </summary>
    private Animator anima = null;

    /// <summary>
    /// Armas de la nave
    /// </summary>
    public GameObject [] armas;

    /// <summary>
    /// Arma activa
    /// </summary>
    public int armaActiva = 0;

    /// <summary>
    /// Indica si esta habilitado el cambio de armas
    /// </summary>
    private bool cambioArmaHabilitado = true;

    /// <summary>
    /// Indica si esta habilitado el mapa
    /// </summary>
    private bool activarMapaHabilitado = true;

    /// <summary>
    /// Vida de la nave
    /// </summary>
    private int vida = 100;

    /// <summary>
    /// Campo de vida
    /// </summary>
    public TextMeshProUGUI campoVida;

    // Start is called before the first frame update
    void Start()
    {
        this.rigid = this.GetComponent<Rigidbody2D>();
        this.anima = this.GetComponent<Animator>();
        this.vida = PartidaEventos.partida.vidaNave;
        Invoke("quitarVida", 15f);
        this.transform.position = new Vector3(PartidaEventos.partida.xNave, PartidaEventos.partida.yNave, 0);
        this.vida = PartidaEventos.partida.vidaNave;
        this.armas[0].GetComponent<Armas>().cantidad = PartidaEventos.partida.arma1;
        this.armas[1].GetComponent<Armas>().cantidad = PartidaEventos.partida.arma2;
    }

    // Update is called once per frame
    void Update()
    {
            limpiarControles();
            procesarJugador();
            sincronizarCamara();
    }

    /// <summary>
    /// Procesa la entrada del jugador
    /// </summary>
    private void procesarJugador()
    {
        
        leerControles();
        if (up)
        {
            acelerar();
        }
        if (down)
        {
            frenar();
        }
        if (right)
        {
            haciaDerecha();
        }
        if (left)
        {
            haciaIzquierda();
        }
        if (menu)
        {
            activarMenu();
        }
        if (action)
        {
            disparar();
        }
        if (secondary)
        {
            cambiarArma();
        }

        if (!up && !down && !left && !right)
        {
            this.anima.SetInteger("Velocidad", 0);
        }

        if(!up && !down && !left && !right)
        {
            rigid.constraints = RigidbodyConstraints2D.FreezeAll;
        }

        campoVida.text = vida + "";

        if(vida <= 0)
        {
            this.anima.SetInteger("Velocidad", -1);
            EventosInterfaces.perder();
        }
    }

    /// <summary>
    /// Quita un punto de vida
    /// </summary>
    private void quitarVida()
    {
        quitarCantidadVida(1);
    }

    /// <summary>
    /// Quita una cantidad de vida 
    /// </summary>
    /// <param name="cantidad"></param>
    private void quitarCantidadVida(int cantidad)
    {
        if (vida > 0)
        {
            vida -= cantidad;
        }
    }

    /// <summary>
    /// Acelera hacia arriba la nave
    /// </summary>
    private void acelerar()
    {
        rigid.velocity = new Vector2(rigid.velocity.x,4.0f);
        this.transform.rotation = Quaternion.Euler(Vector3.forward);
        this.anima.SetInteger("Velocidad", 1);
        rigid.constraints = RigidbodyConstraints2D.FreezePositionX;
    }

    /// <summary>
    /// Acelera hacia abajo la nave
    /// </summary>
    private void frenar()
    {
        rigid.velocity = new Vector2(rigid.velocity.x,-2.0f);
        this.transform.rotation = Quaternion.Euler(Vector3.forward * 180);
        this.anima.SetInteger("Velocidad", 1);
        rigid.constraints = RigidbodyConstraints2D.FreezePositionX;
    }

    /// <summary>
    /// Acelera la nave hacia la derecha
    /// </summary>
    private void haciaDerecha()
    {
        rigid.velocity = new Vector2(2.0f, rigid.velocity.y);
        this.transform.rotation = Quaternion.Euler(Vector3.forward * -90);
        rigid.constraints = RigidbodyConstraints2D.FreezePositionY;
        this.anima.SetInteger("Velocidad", 1);
    }

    /// <summary>
    /// Acelera la nave hacia la izquierda
    /// </summary>
    private void haciaIzquierda()
    {
        rigid.velocity = new Vector2(-2.0f, rigid.velocity.y);
        this.transform.rotation = Quaternion.Euler(Vector3.forward * 90);
        rigid.constraints = RigidbodyConstraints2D.FreezePositionY;
        this.anima.SetInteger("Velocidad", 1);
    }

    /// <summary>
    /// Dispara una bala con el arma seleccionada y su direccion
    /// </summary>
    private void disparar()
    {
        float rotacionZ = this.transform.eulerAngles.z;
        int direccion = 1;

        switch (rotacionZ)
        {
            case 0:
                direccion = 1;
                break;
            case 180:
                direccion = 2;
                break;
            case 270:
                direccion = 3;
                break;
            case 90:
                direccion = 4;
                break;

        }

        armas[armaActiva].GetComponent<Armas>().disparar(direccion);
    }

    /// <summary>
    /// Activa o desactiva el menu
    /// </summary>
    private void activarMenu()
    {
        if (activarMapaHabilitado)
        {
            desactivarActivarMapa();
            EventosInterfaces.mostrarMapa();
        }

    }

    /// <summary>
    /// Cambia de arma 
    /// </summary>
    private void cambiarArma()
    {
        if (cambioArmaHabilitado)
        {
            desactivarCambioArma();
            if (armaActiva == 0)
            {
                armaActiva = 1;
            }
            else
            {
                armaActiva = 0;
            }
        }
    }

    /// <summary>
    /// Sincroniza la camara con el centro de la nave
    /// </summary>
    private void sincronizarCamara()
    {
        if(camaraVinculada != null)
        {
            camaraVinculada.transform.position = new Vector3(this.transform.position.x,this.transform.position.y, camaraVinculada.transform.position.z);
        }
    }

    /// <summary>
    /// Limpia los comandos de la nave
    /// </summary>
    private void limpiarControles()
    {
        up = false;
        down = false;
        left = false;
        right = false;
        action = false;
        secondary = false;
        menu = false;
    }

    /// <summary>
    /// Lee los controles y actualiza los comando de la nave
    /// </summary>
    private void leerControles()
    {
        up = Input.GetKey(KeyCode.W);
        down = Input.GetKey(KeyCode.S);
        left = Input.GetKey(KeyCode.A);
        right = Input.GetKey(KeyCode.D);
        action = Input.GetKey(KeyCode.Space);
        secondary = Input.GetKey(KeyCode.Q);
        menu = Input.GetKey(KeyCode.Tab);
    }

    /// <summary>
    /// Recoge las colisiones con la nave
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.name.ToLower().Contains("asteroide")){
            this.quitarCantidadVida(10);
        }

        if(collision.collider.gameObject.name.ToLower().Contains("sol")
            || collision.collider.gameObject.name.ToLower().Contains("planeta"))
        {
            this.quitarCantidadVida(100);
        }
    }

    /// <summary>
    /// Desactiva el cambio de arma
    /// </summary>
    private void desactivarCambioArma()
    {
        cambioArmaHabilitado = false;
        Invoke("activarCambioArma", 1f);
    }

    /// <summary>
    /// Activa el cambio de arma
    /// </summary>
    private void activarCambioArma()
    {
        cambioArmaHabilitado = true;
    }

    /// <summary>
    /// Desactiva el boton del mapa
    /// </summary>
    private void desactivarActivarMapa()
    {
        activarMapaHabilitado = false;
        Invoke("activarAccionarMapa", 1f);
    }

    /// <summary>
    /// Habilita el boton del mapa
    /// </summary>
    private void activarAccionarMapa()
    {
        activarMapaHabilitado = true;
    }
}
