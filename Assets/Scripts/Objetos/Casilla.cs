using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Casilla : MonoBehaviour
{
    /// <summary>
    /// Cantidad de unidades
    /// </summary>
    public TextMeshProUGUI textoCantidad;

    /// <summary>
    /// Animacion de la casilla
    /// </summary>
    public Animator anima;

    /// <summary>
    /// Inicializacion de animacion
    /// </summary>
    private void Start()
    {
        anima = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(int.Parse(textoCantidad.text) <= 0)
        {
            anima.SetInteger("estado", 1);//se vuelve roja
        }
        else
        {
            anima.SetInteger("estado", 0);//se vuelve azul
        }
    }
}
