using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroide : MonoBehaviour
{
    /// <summary>
    /// vida del asteroide
    /// </summary>
    public int vida = 100;

    /// <summary>
    /// Animaciones del asteroide
    /// </summary>
    Animator anima;

    // Start is called before the first frame update
    void Start()
    {
        anima = this.GetComponent<Animator>();
    }

    // si la vida llega a cero o menos se acciona animacion de explosion que acaba llamando por evento a la funcion destruir()
    void Update()
    {
        if(vida <= 0)
        {
            anima.SetInteger("estado", 1);
        }
    }

    /// <summary>
    /// se destruye y proporciona dinero
    /// </summary>
    public void destruir()
    {
        EventosInterfaces.mostrarPanel(Random.Range(1, 10));
        Destroy(this.gameObject);
    }
}
