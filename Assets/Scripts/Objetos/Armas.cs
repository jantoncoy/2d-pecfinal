using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Armas : MonoBehaviour
{
    /// <summary>
    /// Texto de la cantidad de balas
    /// </summary>
    public TextMeshProUGUI textoAccion;

    /// <summary>
    /// tipo de disparo
    /// </summary>
    public GameObject disparo;

    /// <summary>
    /// cantidad de balas
    /// </summary>
    public int cantidad;

    /// <summary>
    /// Animacion de las armas
    /// </summary>
    public Animator anima;

    /// <summary>
    /// Indica si esta activa
    /// </summary>
    public bool activa;

    /// <summary>
    /// tiempo de recarga del arma
    /// </summary>
    public float tiempoRecarga = 0.5f;

    //inicializamos
    private void Start()
    {
        anima = this.GetComponent<Animator>();
        activa = true;
    }

    /// <summary>
    /// Actualizamos la cantidad de balas
    /// </summary>
    private void Update()
    {
        textoAccion.text = cantidad + "";
    }

    /// <summary>
    /// Disparamos en la direccion proporcionada segun su posicion
    /// </summary>
    /// <param name="direccion"></param>
    public void disparar(int direccion)
    {
        if(cantidad > 0 && activa)
        {
            GameObject bala = null;

            if (direccion == 1)
                bala = Instantiate(disparo, new Vector3(this.transform.position.x, this.transform.position.y + 1, 0), this.transform.rotation);
            if (direccion == 2)
                bala = Instantiate(disparo, new Vector3(this.transform.position.x, this.transform.position.y - 1, 0), this.transform.rotation);
            if (direccion == 3)
                bala = Instantiate(disparo, new Vector3(this.transform.position.x + 1, this.transform.position.y, 0), this.transform.rotation);
            if (direccion == 4)
                bala = Instantiate(disparo, new Vector3(this.transform.position.x - 1, this.transform.position.y, 0), this.transform.rotation);

            if (bala != null)
                bala.GetComponent<Disparo>().direccion = direccion;

            cantidad -= 1;
            recargar();
        }
    }

    /// <summary>
    /// recarga de la bala
    /// </summary>
    public void recargar()
    {
        activa = false;
        Invoke("activar", tiempoRecarga);
    }

    /// <summary>
    /// Activa el disparo
    /// </summary>
    public void activar()
    {
        activa = true;
    }
}
