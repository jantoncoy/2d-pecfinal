using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    /// <summary>
    /// damage de la bala
    /// </summary>
    public int damage = 100;
    /// <summary>
    /// direccion de la bala
    /// </summary>
    public int direccion = 1;
    /// <summary>
    /// velocidad de la bala
    /// </summary>
    public int velocidad = 2;
    /// <summary>
    /// Ejecuciones por segundo de la bala 30
    /// </summary>
    int deteccionInputs = 1000 / 30;//detectaremos
    /// <summary>
    /// Tiempo acumulado hasta proxima ejecucion
    /// </summary>
    float acumulado = 0;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("destruir", 4);
    }

    // Update is called once per frame
    void Update()
    {
        if (acumulado > deteccionInputs)
        {

            if(direccion == 1)
            {
                acelerar();
            }else if(direccion == 2)
            {
                abajo();
            }
            else if (direccion == 3)
            {
                haciaDerecha();
            }
            else if (direccion == 4)
            {
                haciaIzquierda();
            }

            acumulado = 0;
        }
        else
        {
            acumulado += Time.deltaTime / 0.001f;
        }
    }

    /// <summary>
    /// Acelera hacia arriba
    /// </summary>
    private void acelerar()
    {
        this.transform.rotation = Quaternion.Euler(Vector3.forward);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + velocidad, this.transform.position.z);
    }

    /// <summary>
    /// Se mueve hacia abajo
    /// </summary>
    private void abajo()
    {
        this.transform.rotation = Quaternion.Euler(Vector3.forward * 180);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - velocidad, this.transform.position.z);
    }

    /// <summary>
    /// Se muee hacia la derecha
    /// </summary>
    private void haciaDerecha()
    {
        this.transform.rotation = Quaternion.Euler(Vector3.forward * -90);
        this.transform.position = new Vector3(this.transform.position.x + velocidad, this.transform.position.y, this.transform.position.z);
    }

    /// <summary>
    /// hacia la izquierda
    /// </summary>
    private void haciaIzquierda()
    {
        this.transform.rotation = Quaternion.Euler(Vector3.forward * 90);
        this.transform.position = new Vector3(this.transform.position.x - velocidad, this.transform.position.y, this.transform.position.z);
    }

    /// <summary>
    /// destruye la bala
    /// </summary>
    public void destruir()
    {
        Destroy(this.gameObject);
    }

    /// <summary>
    /// Al colisionar se destruye y causa daño en asteroides
    /// </summary>
    /// <param name="collision"></param>
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.name.ToLower().Contains("asteroide"))
        {
            collision.collider.gameObject.GetComponent<Asteroide>().vida -= damage;
        }
        destruir();
    }
}
