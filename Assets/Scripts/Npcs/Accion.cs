using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Accion : MonoBehaviour
{
    /// <summary>
    /// Tipo de accion:
    /// 0 - Dialogo
    /// 1 - Comerciante
    /// 4 - Nave - Salir estacion
    /// </summary>
    public int tipo;
    //Nombre del personaje, item, nace, etc...
    public string nombre;
    //Referencia para el dialogo
    public string referencia;
    //objeto de dialogo
    public NPC npc;

    /// <summary>
    /// Ejecuta la accion segun la configuracion
    /// </summary>
    public void ejecutarAccion()
    {
        switch (tipo)
        {
            case 0:
                npc.comenzarDialogo();
                break;
            case 1:
                //Compra los arreglos de la nave
                if(PartidaEventos.partida.dinero >= 50)
                {
                    PartidaEventos.partida.dinero -= 50;
                    PartidaEventos.partida.arma1 = 20;
                    PartidaEventos.partida.arma2 = 200;
                    PartidaEventos.partida.vidaNave = 120;
                    PartidaEventos.guardar();
                }
                
                npc.comenzarDialogo();
                break;
            case 4:
                //guardado posicion nave
                SceneManager.LoadScene(referencia);
                PartidaEventos.partida.xNave = -20;
                PartidaEventos.partida.yNave = 10;
                PartidaEventos.partida.scenaActual = "Sector_Alfa";
                PartidaEventos.guardar();
                break;
        }
    }
}
