using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Se comone de lo basico al igual que la nave
/// </summary>
public class Player : Basic
{
    /// <summary>
    /// Su propio gameobject (por comodidad)
    /// </summary>
    public GameObject my;

    /// <summary>
    /// indica en que direccion mira
    /// </summary>
    int direccion;

    /// <summary>
    /// La accion que tiene actualmente disponible
    /// </summary>
    Accion accionActual;

    /// <summary>
    /// Indica si esta habilitado el control del usuario
    /// </summary>
    bool habilitado = true;

    // Start is called before the first frame update
    void Start()
    {
        this.rigid = this.GetComponent<Rigidbody2D>();
        this.anima = this.GetComponent<Animator>();
        direccion = 1;//bajar
        Invoke("inicializar", 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        limpiarControles();
        leerControles();
        if (habilitado)
        {
            ejecutarControles();
            sincronizarCamara();
            hayAccion();
        }
        else
        {
            if (action)
            {
                EventosInterfaces.pasarDialogo();
            }
        }
    }

    /// <summary>
    /// Ejecuta el movimiento
    /// </summary>
    private void ejecutarControles()
    {
        this.anima.SetInteger("Estado", 0);

        if (up)
        {
            subir();
        }
        else if (down)
        {
            bajar();
        }
        else if (right)
        {
            derecha();
        }
        else if (left)
        {
            izquierda();
        }else if (action)
        {
            realizarAccion();
        }

        if (!up && !down && !left && !right)
        {
            rigid.constraints = RigidbodyConstraints2D.FreezeAll;
        }

    }

    /// <summary>
    /// Vincula la camara con el npc
    /// </summary>
    private void sincronizarCamara()
    {
        if (camaraVinculada != null)
        {
            camaraVinculada.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, camaraVinculada.transform.position.z);
        }
    }

    /// <summary>
    /// sube hacia arriba
    /// </summary>
    private void subir()
    {
        rigid.velocity = new Vector2(rigid.velocity.x, 2.0f);
        this.anima.SetInteger("Estado", 2);
        rigid.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        direccion = 2;
    }

    /// <summary>
    /// baja hacia abajo
    /// </summary>
    private void bajar()
    {
        rigid.velocity = new Vector2(rigid.velocity.x, -1.0f);
        this.anima.SetInteger("Estado", 1);
        rigid.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        direccion = 1;
    }

    /// <summary>
    /// Se mueve hacia la izquierda
    /// </summary>
    private void izquierda()
    {
        rigid.velocity = new Vector2(-2.0f, rigid.velocity.y);
        this.anima.SetInteger("Estado", 3);
        rigid.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        direccion = 3;
    }

    /// <summary>
    /// Se mueve hacia la derecha
    /// </summary>
    private void derecha()
    {
        rigid.velocity = new Vector2(2.0f, rigid.velocity.y);
        this.anima.SetInteger("Estado", 4);
        rigid.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        direccion = 4;
    }

    //Revisa si esta mirando en la direccion y si se puede realizar alguna accion
    private void hayAccion()
    {
        Vector2 direccionVector = new Vector2(0,0);

        

        if (direccion == 1)
            direccionVector = Vector2.down;
        if (direccion == 2)
            direccionVector = Vector2.up;
        if (direccion == 3)
            direccionVector = Vector2.left;
        if (direccion == 4)
            direccionVector = Vector2.right;

        if(!direccionVector.Equals(new Vector2(0, 0)))
        {
            int layerMask = 1 << 8;
            layerMask = ~layerMask;
            RaycastHit2D golpe = Physics2D.Raycast(transform.position, direccionVector,2,layerMask);
            Debug.DrawRay(this.transform.position, direccionVector*2, Color.green);
            if (golpe.collider != null
                && !golpe.collider.name.ToLower().Contains("protagonista")
                && golpe.collider.gameObject.GetComponent<Accion>() != null)
            {
                accionActual = golpe.collider.gameObject.GetComponent<Accion>();
                EventosInterfaces.mostrarAccionPersonalizado(accionActual.tipo, accionActual.nombre);
                return;
            }
        }
        
        ocultarAccion();
    }

    /// <summary>
    /// Oculta la accion
    /// </summary>
    private void ocultarAccion()
    {
        EventosInterfaces.ocultarAccion();
        accionActual = null;
    }

    //Coge la accion guardada y la ejecuta
    private void realizarAccion()
    {
        if (accionActual != null)
        {
            accionActual.ejecutarAccion();
        }
    }

    /// <summary>
    /// Inicializa eventos importantes
    /// </summary>
    private void inicializar()
    {
        EventosInterfaces.deshabilitarPersonaje = deshabilitarPersonaje;
        EventosInterfaces.habilitarPersonaje = habilitarPersonaje;
    }

    /// <summary>
    /// deshabilita el personaje
    /// </summary>
    public void deshabilitarPersonaje()
    {
        habilitado = false;
    }

    /// <summary>
    /// Habilita el personaje
    /// </summary>
    public void habilitarPersonaje()
    {
        habilitado = true;
    }
}
