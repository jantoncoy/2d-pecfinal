using UnityEngine;

[System.Serializable]
public class Partida {

    /// <summary>
    /// Escena donde se encuentra la nave o jugador
    /// </summary>
    public string scenaActual;

    /// <summary>
    /// Posicion x en la que se encuentra la nave
    /// </summary>
    public float xNave;

    /// <summary>
    /// Posicion y en la que se encuentra la nave
    /// </summary>
    public float yNave;

    /// <summary>
    /// Posicion z en la que se encuentra la nave
    /// </summary>
    public float zNave;

    /// <summary>
    /// Dinero que se tiene
    /// </summary>
    public int dinero;

    /// <summary>
    /// Vida que tiene la nave
    /// </summary>
    public int vidaNave;

    /// <summary>
    /// Balas que tiene arma 1
    /// </summary>
    public int arma1;

    /// <summary>
    /// Balas que tiene arma 2
    /// </summary>
    public int arma2;

    /// <summary>
    /// Constructor basico de la partida
    /// </summary>
    public Partida()
    {
        scenaActual = "Sector_Alfa";
        xNave = 0;
        yNave = -15;
        zNave = 0;
        dinero = 100;
        vidaNave = 100;
        arma1 = 20;
        arma2 = 100;
    }

}
