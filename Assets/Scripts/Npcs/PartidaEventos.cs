using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/// <summary>
/// Clase que contiene los metodos para guardar, cargar y comprobar una partida guardada
/// </summary>
public static class PartidaEventos
{
    /// <summary>
    /// Partida
    /// </summary>
    public static Partida partida = new Partida();

    /// <summary>
    /// indica si la partida fue cargada previamente
    /// </summary>
    public static bool partidaCargada = false;

    /// <summary>
    /// Guarda la partida
    /// </summary>
    /// <returns></returns>
    public static bool guardar()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/partida.gd");
        bf.Serialize(file, partida);
        file.Close();
        Debug.Log(Application.persistentDataPath + "/partida.gd");
        return true;
    }

    /// <summary>
    /// Carga la partida
    /// </summary>
    /// <returns></returns>
    public static bool cargar()
    {
        if (File.Exists(Application.persistentDataPath + "/partida.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/partida.gd", FileMode.OpenOrCreate);
            partida = (Partida)bf.Deserialize(file);
            file.Close();
            Debug.Log(Application.persistentDataPath + "/partida.gd");
            return true;
        }
        return false;
    }

    /// <summary>
    /// Indica si existe una partida
    /// </summary>
    /// <returns></returns>
    public static bool existePartida()
    {
        if (File.Exists(Application.persistentDataPath + "/partida.gd"))
        {
            Debug.Log(Application.persistentDataPath + "/partida.gd");
            return true;
        }
        return false;
    }
}
