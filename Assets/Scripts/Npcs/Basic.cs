using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Modelo en el que se basan las naves
/// </summary>
public class Basic : MonoBehaviour
{
    /// <summary>
    /// Velocidad de la nave (0 = parado, 1 = velocidad normal, 2 = velocidad maxima)
    /// </summary>
    public int velocidad = 0;

    /// <summary>
    /// Indica si es un npc esta nave
    /// </summary>
    public bool isNPC = true;

    /// <summary>
    /// Comandos de la nave
    /// </summary>
    public bool up, down, left, right, action, secondary, menu;

    /// <summary>
    /// Camara vinculada
    /// </summary>
    public GameObject camaraVinculada = null;

    /// <summary>
    /// rigidbody
    /// </summary>
    protected Rigidbody2D rigid = null;

    /// <summary>
    /// Animator
    /// </summary>
    protected Animator anima = null;

    /// <summary>
    /// Limpia los comandos de la nave
    /// </summary>
    protected void limpiarControles()
    {
        up = false;
        down = false;
        left = false;
        right = false;
        action = false;
        secondary = false;
        menu = false;
    }

    /// <summary>
    /// Lee los controles y actualiza los comando de la nave
    /// </summary>
    protected void leerControles()
    {
        up = Input.GetKey(KeyCode.W);
        down = Input.GetKey(KeyCode.S);
        left = Input.GetKey(KeyCode.A);
        right = Input.GetKey(KeyCode.D);
        action = Input.GetKey(KeyCode.Space);
        secondary = Input.GetKey(KeyCode.Q);
        menu = Input.GetKey(KeyCode.Tab);
    }
}
