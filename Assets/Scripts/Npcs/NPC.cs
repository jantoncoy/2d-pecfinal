using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    /// <summary>
    /// Nombre del npc
    /// </summary>
    public string nombre;

    /// <summary>
    /// dialogos que tiene el npc
    /// </summary>
    public string[] dialogos;

    /// <summary>
    /// dialogo actual del npc
    /// </summary>
    public int dialogoActual = -1;

    /// <summary>
    /// Indica si puede continuar avanzando en los dialogos
    /// </summary>
    public bool sePuedePasarDialogo = true;

    /// <summary>
    /// Comienza el dialogo
    /// </summary>
    public void comenzarDialogo()
    {
        if (sePuedePasarDialogo)
        {
            EventosInterfaces.deshabilitarPersonaje();
            EventosInterfaces.pasarDialogo = pasarDialogo;
            if (dialogos != null && dialogos.Length > 0)
            {
                dialogoActual = 0;
                EventosInterfaces.informarDialogo(nombre, dialogos[dialogoActual]);
                sePuedePasarDialogo = false;
                Invoke("esperarDialogo", 1.5f);
                EventosInterfaces.mostrarDialogo();
            }
        }
    }

    /// <summary>
    /// Cuando pasa un tiempo se deja de lectura se deja continuar
    /// </summary>
    public void esperarDialogo()
    {
        sePuedePasarDialogo = true;
    }

    /// <summary>
    /// Continua con el dialogo
    /// </summary>
    public void pasarDialogo()
    {
        if (sePuedePasarDialogo)
        {
            if ((dialogos.Length - 1) == dialogoActual)//cierra el dialogo
            {
                EventosInterfaces.habilitarPersonaje();
                dialogoActual = -1;
                EventosInterfaces.ocultarDialogo();
                sePuedePasarDialogo = false;
                Invoke("esperarDialogo", 1.5f);
            }
            else if (dialogoActual < dialogos.Length)//continua
            {
                dialogoActual++;
                EventosInterfaces.informarDialogo(nombre, dialogos[dialogoActual]);
                sePuedePasarDialogo = false;
                Invoke("esperarDialogo", 1.5f);
            }
        }
    }
}
