using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Estacion : MonoBehaviour
{
    /// <summary>
    /// Nombre de la estacion
    /// </summary>
    public string nombreEstacion;

    /// <summary>
    /// Detectamos las colisiones
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name.ToLower().Contains("ms1"))
        {
            PartidaEventos.partida.xNave = collision.transform.position.x;
            PartidaEventos.partida.yNave = collision.transform.position.z;
            PartidaEventos.partida.scenaActual = nombreEstacion;
            PartidaEventos.guardar();
            SceneManager.LoadScene(nombreEstacion);
        }
    }
}
